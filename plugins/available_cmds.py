from plugin_system import Plugin
from settings import PREFIXES
from settings import ADMINS
plugin = Plugin('Помощь')


@plugin.on_command('команды', 'помоги', 'помощь')
async def call(msg, args):
    usages = ""

    for plugin in msg.vk.get_plugins():
        if not plugin.usage:
            continue

        if plugin.name == 'Контроль бота':
            if not msg.user_id in ADMINS:
                continue

        temp = "🔷" + plugin.name + "\n"

        for usage in plugin.usage:
            temp += "🔶" + PREFIXES[0] + usage + "\n"

        temp += "\n"

        if len(usages) + len(temp) >= 3072:
            await msg.answer(usages, True)
            usages = ""

        usages += temp

    await msg.answer(usages, True)
