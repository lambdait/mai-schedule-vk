import re

import requests
from bs4 import BeautifulSoup

from database import *
from plugin_system import Plugin

REG_HELP = '''регистрация, рег [код группы]
    Команда запишет Вас в память :)
    Примеры команды:
    ✔ регистрация 3-М5О-201Б-16
    ✔ рег М3О-119Бк-17
    '''
FAC_HELP = '''факультет, ф [номер факультета], где номер факультета:
        1 - Факультет 1
        2 - Факультет 2
        3 - Факультет 3
        4 - Факультет 4
        5 - ИНЖЭКИН
        6 - Факультет 6
        7 - Факультет 7
        8 - Факультет 8
        9 - Факультет 9
        11 - Институт №11
        12 - Институт №12
        13 - Факультет ФИЯ
    Примеры команды:
    ✔ факультет 5
    ✔ ф 1
    '''
COURSE_HELP = '''курс, к [номер курса]
    Команда запишет Ваш курс. Используйте число от 1 до 6
    Примеры команды:
    ✔ курс 2
    ✔ курс 3
    '''
PROG_HELP = '''программа, прог, п [программа обучения]
    Команда запишет Вашу программу обучения. Доступны:
    - 'б' - Бакалавриат
    - 'м' - Магистратура
    - 'с' - Специалитет
    Примеры команды:
    ✔ программа б
    ✔ программа м
    ✔ прог б
    ✔ п с
    '''
GROUP_HELP = '''группа, гр
    Команда покажет список групп, подходящих под Ваши параметры. Выберите свою группу, скопируйте сообщение и отправьте боту.
    Уточнить номер группы можно тут -> https://mai.ru/education/schedule/
    '''

USAGE = [
    'Первый способ:',
    REG_HELP,
    '''Второй способ:
    Внимание! Если со способом номер два проблемы, воспользуйтесь первым способом
    По очереди вводите свои данные согласно инструкции
    ''',
    FAC_HELP,
    COURSE_HELP,
    PROG_HELP,
    GROUP_HELP
]

plugin = Plugin('Регистрация',
                usage=USAGE)


@plugin.on_command('факультеты', 'фак', 'фк', 'факультет', 'ф')
async def reg_faculty(msg, args):
    continue_message = '''
        Факультет записан :)
        Чтобы продолжить регистрацию наберите\n
        ''' + COURSE_HELP
    await process_staged_registration(msg, args, FAC_HELP, fac_stage, continue_message)


@plugin.on_command('курс', 'к')
async def reg_course(msg, args):
    continue_message = '''
        Курс записан :)
        Чтобы продолжить регистрацию наберите\n
        ''' + PROG_HELP
    await process_staged_registration(msg, args, COURSE_HELP, course_stage, continue_message)


@plugin.on_command('программа', 'прог', 'п')
async def reg_program(msg, args):
    continue_message = '''
        Программа записана :)
        Чтобы продолжить регистрацию наберите\n
        ''' + GROUP_HELP
    await process_staged_registration(msg, args, PROG_HELP, program_stage, continue_message)


@plugin.on_command('группа', 'группы', 'гр')
async def reg_group(msg, args):
    user = await get_or_none(User, uid=msg.user_id)

    if not user:
        return await msg.answer('База данных бота не настроена')

    faculty = user.faculty
    course = user.course
    user_program = user.program
    groups_list_url = f'https://mai.ru/education/schedule/?department={faculty}&course={course}'

    groups = get_groups_codes(groups_list_url, user_program)

    if groups:
        await msg.answer('Найдите свою группу в этом списке:')
        for group in groups:
            await msg.answer('рег ' + group)
    else:
        return await msg.answer('Что-то пошло не так, возможно, на сайте МАИ проблемы')

    return await msg.answer(f'Скопируте сообщение с кодом вашей группы и отправьте боту')


@plugin.on_command('рег', 'регистрация', 'registration')
async def reg_user(msg, args):
    continue_message = '''Вы записаны :)
    Если будет приходить пустое расписание, попробуйте зарегестрироваться заново либо сообщите админам
    '''
    await process_staged_registration(msg, args, REG_HELP, registration_stage, continue_message)


async def process_staged_registration(msg, args, help_text, stage, continue_message):
    user = await get_or_none(User, uid=msg.user_id)

    if not user:
        return await msg.answer('База данных бота не настроена')

    if not args:
        return await msg.answer('Введите команду согласно справке:\n' + help_text)

    error_message = stage(args, user)

    if error_message:
        return await msg.answer(error_message + '\n' + help_text)

    await db.update(user)
    await msg.answer(continue_message)


def fac_stage(args, user):
    faculties_codes = {
        1: 150,
        2: 153,
        3: 157,
        4: 149,
        5: 155,
        6: 160,
        7: 154,
        8: 151,
        9: 152,
        11: 165,
        12: 164,
        13: 168
    }

    try:
        faculty = int(args[0])
    except ValueError:
        return 'Неправильный код факультета'

    if faculty in faculties_codes:
        faculty = faculties_codes[faculty]
    else:
        return 'Неправильный код факультета'

    user.faculty = faculty


def course_stage(args, user):
    course = args[0]

    try:
        if not 1 <= int(course) <= 6:
            return 'Курс должен быть от 1 до 6'
    except ValueError:
        return 'Курс должен быть числом'

    user.course = course


def program_stage(args, user):
    program_codes = {
        'б': 'Бакалавриат',
        'м': 'Магистратура',
        'с': 'Специалитет'
    }

    program = str(args[0]).lower()

    if program in program_codes:
        program = program_codes[program]
    else:
        return 'Неправильная программа обучения'

    user.program = program


def registration_stage(args, user):

    group = args[0].upper()

    replacements = ('БКИ', 'БК', 'БИ',
                    'МКИ', 'МК', 'CКИ', 'СК')
    for repl in replacements:
        group = group.replace(repl, repl.capitalize())

    user.group = group


def get_groups_codes(groups_list_url, user_program):
    try:
        req = requests.get(groups_list_url)
    except Exception as e:
        return None

    req.encoding = 'utf-8'

    soup = BeautifulSoup(req.text, 'html.parser')
    programs = soup.find_all('div', {'class': 'sc-program'})

    for program in programs:
        if program.text == user_program:
            groups = program.find_next_siblings('a')
            groups = [group.text for group in groups]
            return groups
