import re
from urllib.error import URLError
from urllib.parse import quote
from urllib.request import urlopen

from database import *
from plugin_system import Plugin


class IncorrectRequest(Exception):
    def __init__(self, error_message):
        self.error_message = error_message
        self.help_text = 'Если понадобится список доступных команд — отправьте команду "помощь"'
        Exception.__init__(self, f'{error_message}\n{self.help_text}')


def get_time_couples():
    start_times = [
        (9, 0), (10, 45),
        (13, 0), (14, 45),
        (16, 30), (18, 15)
    ]
    end_times = [
        (10, 30), (12, 15),
        (14, 30), (16, 15),
        (18, 00), (19, 45)
    ]
    start_times = [datetime.time(*time) for time in start_times]
    end_times = [datetime.time(*time) for time in end_times]

    couples = zip(start_times, end_times)
    return list(couples)


TIME_COUPLES = get_time_couples()

WEEK_DAYS = {
    'пн': 1, 'понедельник': 1,
    'вт': 2, 'вторник': 2,
    'ср': 3, 'среда': 3,
    'чт': 4, 'четверг': 4,
    'пт': 5, 'пятница': 5,
    'сб': 6, 'суббота': 6
}

WORDS = {
    'с': 0,
    'сегодня': 0,
    'з': 1,
    'завтра': 1,
    'послезавтра': 2
}

USAGE = [
    """расписание, р [день] - расписание на выбранную дату
    Примеры команд:
    ✔ расписание пн
    ✔ р понедельник
    ✔ расписание 21
    ✔ р 21.10
    ✔ расписание завтра
    ✔ р сегодня 
    """,
    """пара - узнать текущую или следующую пару
    Примеры команд:
    ✔ пара сейчас
    ✔ пара следующая
    """
]
plugin = Plugin('Расписание пар',
                usage=USAGE)


@plugin.on_command('р', 'расписание', 'schedule')
async def schedule_command(msg, args):
    await handle_general_request(msg, args, handle_schedule_request)


@plugin.on_command('пара', 'para')
async def lesson_command(msg, args):
    await handle_general_request(msg, args, handle_lesson_request)


async def handle_general_request(msg, args, handler):
    user = await get_or_none(User, uid=msg.user_id)
    group = user.group
    if not user:
        await msg.answer('Вас не существует или база данных бота не настроена')
    elif not group:
        await msg.answer('Повторите регистрацию')
    elif args:
        try:
            schedule = get_schedule(group)
        except URLError:
            return await msg.answer(f'Неверный номер группы "{group}", повторите регистрацию')
        request = ' '.join(args).lower()
        message = handler(request, schedule)
        await msg.answer(message)
    else:
        await msg.answer('Некорректный запрос')


def get_schedule(group) -> list:
    """
    :param group: string representing student's group
    :return schedule: list of dicts <3
    """
    marks = ['date', 'dow', 'start_time', 'end_time',
             'title', 'lecturer', 'place', 'type']
    group = quote(group)
    url = f"https://mai.ru/education/schedule/data/{group}.txt"

    response = urlopen(url)
    response = response.read()
    schedule = response.decode('utf-8-sig')
    schedule = list(set(schedule.split('\n')))
    schedule = [data.split('\t') for data in schedule]
    schedule = [dict(zip(marks, lesson)) for lesson in schedule]

    return schedule


def handle_lesson_request(request, schedule) -> str:
    answer = ''
    current_time = datetime.datetime.now().time()
    try:
        time = convert_time_request(request, current_time)
        lesson = search_for_time(time, schedule)
        if not lesson:
            answer = 'Кажется, пары нет :)'
        else:
            answer = prepare_message(lesson)
    except IncorrectRequest as e:
        header = 'Ошибка\n'
        answer = str(e)
    else:
        header = f'{request.capitalize()}\n=========\n'

    return header + answer


def convert_time_request(request, current_time) -> str:
    if request in ['сейчас']:
        time = get_time_for_current(current_time)
    elif request in ['следующая']:
        time = get_time_for_next(current_time)
    else:
        raise IncorrectRequest(f'Запрос "{request}" не поддерживается')
    time = str(time)
    if time.startswith('0'):
        time = time[1:]
    return str(time)


def get_time_for_current(current_time):
    time = datetime.time(0)

    for start, end in TIME_COUPLES:
        if start <= current_time <= end:
            time = start

    return time


def get_time_for_next(current_time):
    time = datetime.time(0)

    for start, end in TIME_COUPLES:
        if current_time <= start:
            time = start
            break

    return time


def search_for_time(time, schedule) -> dict:
    date = datetime.date.today().strftime('%d.%m.%Y')
    lessons = search_for_date(date, schedule)
    required_lesson = None

    for lesson in lessons:
        if lesson.get('start_time') == time:
            required_lesson = lesson

    return required_lesson


def handle_schedule_request(request, schedule) -> str:
    answer = ''

    try:
        date = convert_data_request(request)
        lessons = search_for_date(date, schedule)
        for lesson in lessons:
            answer += prepare_message(lesson)
    except IncorrectRequest as e:
        header = 'Ошибка\n'
        answer = str(e)
    else:
        header = f'Расписание на {request}, {date}\n=========\n'

    return header + answer


def convert_data_request(request) -> str:
    today = datetime.date.today()

    if request in WORDS:
        date = convert_word(request, today)
    elif request in WEEK_DAYS:
        date = convert_week_day(request, today)
    elif re.fullmatch(r'\d{1,2}(\.\d{1,2}){0,4}', request):
        date = convert_date(request, today)
    else:
        raise IncorrectRequest(f'Запрос "{request}" не поддерживается')

    return date.strftime('%d.%m.%Y')


def convert_word(word, today):
    delta = WORDS[word]
    date = today + datetime.timedelta(days=delta)

    return date


def convert_week_day(day_of_week, today):
    today = today.strftime('%u %W %y').split()
    curr_day_of_week, curr_week, curr_year = map(int, today)
    required_day = WEEK_DAYS[day_of_week]

    if required_day > curr_day_of_week:
        date_string = f'{required_day} {curr_week} {curr_year}'
    else:
        next_week = curr_week + 1
        date_string = f'{required_day} {next_week} {curr_year}'

    date = datetime.datetime.strptime(date_string, '%u %W %y').date()

    return date


def convert_date(date, today):
    date_parts = list(map(int, date.split('.')))

    if len(date_parts) >= 3 and date_parts[2] < 2000:
        date_parts[2] += 2000

    keys = ['day', 'month', 'year']
    kwargs = dict(zip(keys, date_parts))

    try:
        date = today.replace(**kwargs)
    except ValueError:
        raise IncorrectRequest(f'Некорректная дата {date}')

    return date


def search_for_date(date, schedule) -> list:
    lessons = [lesson for lesson in schedule if lesson['date'] == date]
    lessons.sort(key=lambda lesson: int(lesson['start_time'].split(':')[0]))

    return lessons


def prepare_message(lesson) -> str:
    incorrect_inputs = ['NONAME NONAME ', '', ' ']
    tag_groups = [
        ['start_time', 'end_time'],
        ['title', 'type'],
        ['lecturer'],
        ['place']
    ]
    formatters = (
        "⌚ {0} -- {1}",
        "📝 {0}, {1}",
        "👤 {0}",
        "📍 {0}"
    )
    lesson_data = []

    for tag_group in tag_groups:
        lesson_data_line = []
        for tag in tag_group:
            if not lesson[tag] in incorrect_inputs:
                lesson_data_line.append(lesson[tag])
        lesson_data.append(lesson_data_line)

    couples = zip(formatters, lesson_data)
    message = [couple[0].format(*couple[1]) for couple in couples if couple[1]]
    message = '\n'.join(message) + '\n\n'

    return message
