from settings import TOP_ADMIN
from plugin_system import Plugin

USAGE = """админу [ваше сообщение]
            Примеры команды:
    ✔ админу <3 
    ✔ админу почему нихрена не работает, а, а?
        """
plugin = Plugin('Обратная связь',
                usage=USAGE)


@plugin.on_command('админу')
async def reg_faculty(msg, args):
    sender_id = msg.user_id
    message = ' '.join(args)
    val = {
        'peer_id': TOP_ADMIN,
        'message': f'{message}\nid{sender_id}'
    }
    result = await msg.vk.method('messages.send', val)
