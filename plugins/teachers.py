from peewee import *
from plugin_system import Plugin
import os

directory = os.path.dirname(os.path.abspath(__file__))
db = SqliteDatabase(directory + '/teachers.db')

class Teacher(Model):
    name = CharField()
    surename = CharField()
    patronymic = CharField()
    guid = CharField()

    class Meta:
        database = db

USAGE = '''препод [фамилия] [имя] [отчество]
    Команда отправит вам ссылку на расписание препода, если он есть в базе данных.
    Обязательно нужно ввести фамилию, если нужно уточнить, можно дописать имя и отчество, но только в таком порядке
    Если не знаете фамилию или имя - используйте знак дефиса '-' вместо него
    Примеры команды:
    ✔ препод Корунов
    ✔ препод Корунов Евгений
    ✔ препод Корунов Евгений Олегович
    ✔ препод Корунов - Олегович
    ✔ препод - Евгений Олегович
    '''

plugin = Plugin('Расписание препода',
                usage=USAGE)

@plugin.on_command('препод')
async def teacher_command(msg, args):
    args_count = len(args)

    if args_count > 3 or args_count < 1:
        return await msg.answer('Воспользуйтесь командой согласно справке\n' + USAGE)

    args = [arg.capitalize() for arg in args]

    condition = ~(Teacher.name >> None)
    fields = (Teacher.surename, Teacher.name, Teacher.patronymic)
    for name_part, field in zip(args, fields):
        if not name_part == '-':
            condition = condition & (field == name_part)

    answer = get_teachers_info_message(condition)

    return await msg.answer(answer)

def get_teachers_info_message(condition):
    teachers = Teacher.select().where(condition)

    if not teachers:
        message = 'В базе данных нет такого препода'
    else:
        message = 'Вот что нашли:\n'
        for teacher in teachers:
            message += create_teacher_info(teacher)
    
    return message


def create_teacher_info(teacher):
    link_pattern = 'https://mai.ru/education/schedule/ppc.php?guid='

    surename = teacher.surename
    name = teacher.name
    patronymic = teacher.patronymic
    guid = teacher.guid

    link = link_pattern + guid
    message = f'''------------
                {surename} {name} {patronymic}
                {link}
                '''
    
    return message
